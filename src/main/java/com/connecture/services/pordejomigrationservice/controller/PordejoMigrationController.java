package com.connecture.services.pordejomigrationservice.controller;


import java.util.List;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.connecture.services.pordejomigrationservice.service.MigrationService;
import com.fasterxml.jackson.databind.JsonNode;

@RestController
public class PordejoMigrationController
{
  @Autowired
  private MigrationService migrationService;
  
  @GetMapping("/health")
  public String health() {
    return "Pordejo Migration Service is operational";
  }
  
  @PostMapping("/migrate")
  public JsonNode migrate(
    @QueryParam(value = "version") String version,
    @RequestBody JsonNode oldWorkflow)
  {
    return migrationService.migrate(oldWorkflow, version);
  }
  
  @PostMapping("/migrateList")
  public List<JsonNode> migrateList(
    @QueryParam(value = "version") String version,
    @RequestBody List<JsonNode> oldWorkflows)
  {
    return migrationService.migrateList(oldWorkflows, version);
  }
}
