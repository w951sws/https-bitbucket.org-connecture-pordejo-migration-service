package com.connecture.services.pordejomigrationservice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class MigrationService
{
  private ObjectMapper mapper = new ObjectMapper();
  
  public List<JsonNode> migrateList(List<JsonNode> oldWorkflows, String version)
  {
    List<JsonNode> newWorkflows = new ArrayList<>();
    for (JsonNode oldWorkflow : oldWorkflows)
    {
      newWorkflows.add(migrate(oldWorkflow, version));
    }    
    return newWorkflows;
  }
  
  public JsonNode migrate(JsonNode oldWorkflow, String version)
  {
    ObjectNode newWorkflow = mapper.createObjectNode();
    newWorkflow.set("workflowId", null);
    newWorkflow.put("tenant", oldWorkflow.path("clientIdentifier").asText());
    newWorkflow.put("type", "workflow");
    newWorkflow.put("revision", 0);
    newWorkflow.put("version",(version == null) ? "" : version);
    
    ObjectNode configuration = mapper.createObjectNode();
    newWorkflow.set("configuration", configuration);
    configuration.put("uri", oldWorkflow.path("uri").asText());
    configuration.put("method", oldWorkflow.path("method").asText());
    configuration.set("alias", null);
    configuration.set("originalId", null);
    configuration.put("description", oldWorkflow.path("description").asText());
    
    ObjectNode workflow = mapper.createObjectNode();
    configuration.set("workflow", workflow);
    workflow.set("tags", mapper.createArrayNode());
    
    ArrayNode newSteps = mapper.createArrayNode();
    workflow.set("steps", newSteps);
    ArrayNode oldSteps = (ArrayNode)oldWorkflow.get("workflow").get("steps");
    for (int i = 0; i < oldSteps.size(); i++)
    {
      JsonNode oldStep = oldSteps.get(i);
      String stepType = oldStep.get("stepType").asText();
      if ("SCRIPT".equalsIgnoreCase(stepType))
      {
        newSteps.add(migrateScriptStep(oldStep));
      }
      else if ("REST".equalsIgnoreCase(stepType))
      {
        newSteps.add(migrateRestStep(oldStep));
      }
      else if ("PROXY".equalsIgnoreCase(stepType))
      {
        newSteps.add(migrateRestStep(oldStep));
      }
      else
      {
        throw new IllegalArgumentException(String.format("Unrecognized step type: %s", stepType));
      }
    }
    
    return newWorkflow;
  }
  
  protected JsonNode migrateRestStep(JsonNode oldStep)
  {
    ObjectNode newStep = mapper.createObjectNode();
    newStep.put("description", oldStep.path("name").asText());
    newStep.put("stepType", "REST");
    newStep.set("name", null);
    newStep.put("httpMethod", oldStep.path("httpMethod").asText());
    newStep.put("endpointUri", oldStep.path("endpointUri").asText());
    newStep.put("conditional", (oldStep.has("conditional") ? oldStep.get("conditional").asText() : null));
    newStep.set("multipartUpload", null);
    newStep.set("includeHeaders", (oldStep.has("includeHeaders") ? oldStep.get("includeHeaders") : mapper.createArrayNode()));
    newStep.set("includeBodyParams", mapper.createArrayNode());
    newStep.set("allowedResponseCodes", (oldStep.has("allowedResponseCodes") ? oldStep.get("allowedResponseCodes") : mapper.createArrayNode()));
    newStep.set("inputAlias", null);
    newStep.set("outputAlias", null);
    newStep.put("contentType", "application/json");
    return newStep;
  }
  
  protected JsonNode migrateScriptStep(JsonNode oldStep)
  {
    ObjectNode newStep = mapper.createObjectNode();
    newStep.put("description", oldStep.path("name").asText());
    newStep.put("stepType", "SCRIPT");
    newStep.set("name", null);
    newStep.put("scriptText", oldStep.path("scriptText").asText());
    newStep.put("parseScriptAsJson", true);
    newStep.set("inputAlias", null);
    newStep.set("outputAlias", null);
    return newStep;
  }
}
